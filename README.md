# SQLServerSnapshot

Using SNAPSHOT ISOLATION level with TRANSACTION support to enable shadow updates in Microsoft SQL Server. The POC is implemented using Microsoft Visual C++, Microsoft Visual F#. Microsoft Visual C# and Microsoft Visual Basic .NET

For latest updates, please visit the CodePlex source control at http://sqlserversnapshot.codeplex.com/. These updates will be pushed to this GitHub repository eventually...
